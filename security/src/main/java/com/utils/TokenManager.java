package com.utils;

import io.jsonwebtoken.CompressionCodec;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/*
* token管理
*/
public class TokenManager {

    private final long nowMillis = System.currentTimeMillis();

    /*
    * 对应 JwtConstants.SECRET 属性
    * 可以由系统生成
    * https://blog.csdn.net/why15732625998/article/details/78534711*/
//    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

    /**
    * @ref https://www.jianshu.com/p/1ebfc1d78928?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
    * @param uname 用户名
    * @return token
    */
    public String createToken(String uname){
        String token = Jwts.builder().setSubject(uname)
                .setExpiration(new Date(nowMillis + JwtConstants.TIME_OUT))
                //签名方法  两个参数分别是签名算法和自定义的签名Key（盐）
                .signWith(SignatureAlgorithm.HS512,JwtConstants.SECRET)
                //当载荷过长时可对其进行压缩
                .compressWith(CompressionCodecs.GZIP)
                //生成JWT
                .compact();
        return token;
    }

        /*
        * 感觉这里应该是获取 rolename，不是user  < getBody()后得到的其实是 claim >
        *
        * 因为有这样一段关于 Claims认证的介绍
        *  Claims认证的其中优点之一是:比如你有很多个用户具有相同的权限，
        * 那么对于这些用户，只需要根据他们的角色产生一个claims token,而没有必要产生很多个token,
        *
        * @ref 关于jwt token鉴权的一些理解 https://blog.csdn.net/xielinze/article/details/90752886
        * */
    public String getUserFromToken(String token){
        String uname = Jwts.parser().setSigningKey(JwtConstants.SECRET)
                .parseClaimsJws(token)
                .getBody().getSubject();
        return uname;
    }

    public void removeToken(String token) {
    }

}