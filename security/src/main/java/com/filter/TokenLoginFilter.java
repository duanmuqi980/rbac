package com.filter;

import com.entity.SecurityUser;
import com.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utils.ResponseUtil;
import com.utils.TokenManager;
import lombok.SneakyThrows;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/*
* 登录过滤器 <对用户名密码进行登录校验 >
*/
public class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private TokenManager tokenManager;
    private RedisTemplate redisTemplate;

    public TokenLoginFilter(AuthenticationManager authenticationManager, TokenManager tokenManager, RedisTemplate redisTemplate) {
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
        this.setPostOnly(false);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/admin/acl/login","POST"));
    }

    /*
    * 尝试认证 <身份验证 >
    */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        try {
            User user = new ObjectMapper().readValue(req.getInputStream(), User.class);
            /*
             * 将用户表单提交过来的用户名和密码
             * 封装成对象委托类 AuthenticationManager
             * 用验证方法 authenticate() 进行身份验证
             */
            /*
             * UsernamePasswordAuthenticationToken
             * 从HttpRequest中获取对应的参数字段，并将其封装进Authentication中
             */
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /*
    * 认证成功
    */
    @SneakyThrows
    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException{
        // Authentication.getPrincipal()可以获取到代表当前用户的信息，这个对象通常是UserDetails的实例
        SecurityUser securityUser = (SecurityUser) auth.getPrincipal();
        String token = tokenManager.createToken(securityUser.getUsername());
        redisTemplate.opsForValue().set(securityUser.getUsername(),securityUser.getPermissionList());
        ResponseUtil.out(res,"s");
    }

    /*
    * 认证失败
    */
    @SneakyThrows
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException e) throws IOException, ServletException {
        ResponseUtil.out(response, "s");
    }
}
