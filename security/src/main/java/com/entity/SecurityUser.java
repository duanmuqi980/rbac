package com.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class SecurityUser implements UserDetails {
//    当前用户
    private transient User currentUserInfo;
//    权限列表
    private List<String> permissionList;

    public SecurityUser(User user){
        if(user != null){
            this.currentUserInfo = user;
        }
    }
    public SecurityUser(){}

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
//        这个属性中存储了这个用户所有的权限
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for(String permission : permissionList){
            if(StringUtils.isEmpty(permission)){ continue; }
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permission);
            authorities.add(authority);
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return currentUserInfo.getPassword();
    }

    @Override
    public String getUsername() {
        return currentUserInfo.getUsername();
    }

    //账户没有过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    //账户没有被锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    //证书没有过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    //账户是否有效
    @Override
    public boolean isEnabled() {
        return true;
    }
}

