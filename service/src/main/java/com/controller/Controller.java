package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.service.MService;
import com.utils.Result;
import com.utils.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/admin/service/index")
public class Controller {

    @Resource
    MService mService;

    /*
    * 获取用户信息
    */
    @GetMapping("/userInfo")
    public Result getUserInfo(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> userInfo = mService.getUserInfo(username);
        return new Result<Map>(true,StatusCode.OK,"已获取用户信息",userInfo);
    }
}
