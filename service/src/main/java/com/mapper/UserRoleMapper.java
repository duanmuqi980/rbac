package com.mapper;

import com.entity.UserRoleEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {
}
