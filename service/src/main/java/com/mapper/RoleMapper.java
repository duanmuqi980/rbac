package com.mapper;

import com.entity.RoleEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface RoleMapper extends BaseMapper<RoleEntity> {
}
