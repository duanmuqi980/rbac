package com.mapper;

import com.entity.RolePermissionEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface RolePermissionMapper extends BaseMapper<RolePermissionEntity> {
}
