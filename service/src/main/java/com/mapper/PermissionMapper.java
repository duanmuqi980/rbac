package com.mapper;

import com.entity.PermissionEntity;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface PermissionMapper extends BaseMapper<PermissionEntity> {
}
