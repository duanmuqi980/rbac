package com.mapper;

import com.entity.UserEntity;
import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<UserEntity> {
}
