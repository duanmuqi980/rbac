package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.entity.PermissionEntity;
import com.entity.UserEntity;
import com.mapper.PermissionMapper;
import com.mapper.UserMapper;
import com.service.PermissionService;
import com.sun.javafx.tk.PermissionHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<String> selectPermissionValueByUserId(Integer id) {
        List<String> permissionValueList = null;
        List<PermissionEntity> temp = null;
        if(this.isAdmin(id)){
            temp = permissionMapper.selectAll();
            for(PermissionEntity p:temp){
                String permission = p.toString();
                permissionValueList.add(permission);
            }
            return permissionValueList;
        }
        else {
            permissionValueList.add(permissionMapper.selectByPrimaryKey(id).toString());
        }
        return permissionValueList;
    }

//    @Override
//    public List<JSONObject> selectPermissionByUserId(Integer userId) {
//        List<PermissionEntity> selectPermissionList;
//        List<String> PermissionList;
//        if(this.isAdmin(userId)) {
//            //如果是超级管理员，获取所有菜单
//            selectPermissionList = permissionMapper.selectAll();
//        } else {
//            selectPermissionList = permissionMapper.selectByPrimaryKey(userId);
//        }
//
//        List<Permission> permissionList = PermissionHelper.bulid(selectPermissionList);
//        List<JSONObject> result = MemuHelper.bulid(permissionList);
//        return result;
//    }


    /*
    * 判断是否为管理员
    */
    private boolean isAdmin(Integer uid){
        UserEntity user = userMapper.selectByPrimaryKey(uid);
        if(null != user && "admin".equals(user.getName())){
            return true;
        }
        return false;
    }
}
