package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.entity.RoleEntity;
import com.entity.UserEntity;
import com.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MServiceImpl<U extends BaseMapper<UserEntity>, U1> implements MService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 根据用户名获取用户登录信息
     *
     * @param username
     * @return
     */
    public Map<String, Object> getUserInfo(String username) {
        Map<String, Object> result = new HashMap<>();
        UserEntity user = userService.selectByUserName(username);
        //根据用户id获取角色名
        String roleName = userRoleService.getRoleByUserId(user.getUid()).getName();
        //根据用户id获取操作权限值
        List<String> permissionValueList = permissionService.selectPermissionValueByUserId(user.getUid());
        redisTemplate.opsForValue().set(username, permissionValueList);

        result.put("name", user.getName());
        result.put("role name", roleName);
        result.put("permissionValueList", permissionValueList);
        return result;
    }

}
