package com.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.entity.UserEntity;
import com.mapper.UserMapper;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import tk.mybatis.mapper.entity.Example;


/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2021-03-18 10:26:23
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserEntity selectByUserName(String username) {
        Example example = new Example(UserEntity.class);
        example.createCriteria().andEqualTo("name",username);
        UserEntity user = userMapper.selectOneByExample(example);
        return user;
    }
}
