package com.service;

import com.alibaba.fastjson.JSONObject;
import com.entity.PermissionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Permission)表服务接口
 */
@Service
public interface PermissionService {

    // 根据用户id获取用户权限值
    List<String> selectPermissionValueByUserId(Integer id);

}
