package com.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient(name = "provider")
public interface MService {
    /**
     * 根据用户名获取用户登录信息
     * @param uname
     * @return
     */
    @GetMapping("provider/get/userinfo/{name}")
    Map<String, Object> getUserInfo(@PathVariable("name") String uname);
}
